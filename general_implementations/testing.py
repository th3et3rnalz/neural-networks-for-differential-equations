from general_first_order_ode_solver import ODESolver, LinearLayer
import matplotlib.pyplot as plt
import jax.numpy as jnp
import numpy as np
import jax.nn as f_act


def bc_func(x, y):
    return x + x*x*y


def ode_func(y_xx, y_x, y, x):
    return y_xx + y_x/5 + y + jnp.exp(-x/5)*jnp.cos(x)/5


layers = [LinearLayer(1, activation=f_act.sigmoid),
          LinearLayer(50, activation=f_act.sigmoid),
          LinearLayer(50, activation=f_act.sigmoid),
          LinearLayer(1)]


nn = ODESolver(layers=layers, ode_func=ode_func, bc_func=bc_func, title="Problem_3_paper", order=2, use_wandb=True)
nn.momentum = 0.5
nn.bA_beta = 0.5
nn.alpha_initial = 0.1

x_train = jnp.linspace(0, 2, 1024).reshape(-1, 1)
n_iterations = 500
loss_arr = []
alpha_arr = []
intermediate_steps = []


for i in range(n_iterations):
    loss_i, alpha_i = nn.learn(x_train)
    loss_arr.append(loss_i)
    alpha_arr.append(alpha_i)

    if i % 10 == 0:
        print(i, loss_arr[-1])
        intermediate_steps.append(jnp.asarray(nn.forward(x_train))[:, 0])

np.savetxt('../basics/data.txt', jnp.array(intermediate_steps))

plt.subplot(2, 1, 1)
plt.plot(x_train, nn.forward(x_train), label="Neural network")
plt.plot(x_train, jnp.exp(-x_train/5)*jnp.sin(x_train), label="Exact")
plt.grid()
plt.legend()
plt.title("Final predictions")

plt.subplot(2, 2, 3)
plt.title("Loss progression")
plt.semilogy(loss_arr)
plt.xlabel("Epochs")

plt.subplot(2, 2, 4)
plt.title("Step size progression")
plt.semilogy(alpha_arr)
plt.xlabel("Epochs")

plt.tight_layout()
plt.show()