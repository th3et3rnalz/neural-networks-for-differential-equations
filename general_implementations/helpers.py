from typing import List
from jax import random
import jax.numpy as jnp
import warnings
import jax.nn
from random import randint
from copy import deepcopy


class GenericBoundaryConditions:
    x_a = 0
    x_b = 1
    f_a = 0
    f_b = 0

    def enforce(self, x, y):
        """
        This is the function, meant to be applied to the output of a neural network, that will ensure
        that the boundary conditions are always met.

        :return:
        """
        warnings.warn("Boundary condition enforcement wasn't overridden, check implementation BC class")
        return y  # to be overridden


class GenericLayer:
    activation = jax.nn.sigmoid
    n_neurons = 0


def norm_grad_f_squared(g_val):
    norm_val = 0
    for a, b in g_val:
        norm_val += jnp.linalg.norm(a) ** 2 + jnp.linalg.norm(b) ** 2
    return norm_val


def update_w_b(w_b, step_size, g_val, w_b_previous=None, beta=None):
    if w_b_previous is None:
        for i in range(len(w_b)):
            w_b[i][0] += -step_size * g_val[i][0]
            w_b[i][1] += -step_size * g_val[i][1]
    else:
        if beta is None:
            beta = step_size / 2
        for i in range(len(w_b)):
            w_b[i][0] += -step_size * g_val[i][0] + beta*(w_b[i][0] - w_b_previous[i][0])
            w_b[i][1] += -step_size * g_val[i][1] + beta*(w_b[i][1] - w_b_previous[i][1])
    return w_b


class LinearLayer(GenericLayer):
    def __init__(self, number_of_neurons, activation=lambda x: x):
        """
        A simple linear layer, purely create for easy network initialization.

        :param number_of_neurons: The number of neurons in this specific layer
        :param activation: Activation function, default to linear.
        """
        self.n_neurons = number_of_neurons
        self.activation = activation


class GeneralNeuralNetwork:
    def __init__(self, layers: List[GenericLayer], bc_func, title: str, key=jax.random.PRNGKey(randint(0, 2**16-1)),
                 use_wandb=True):
        self.use_wandb = use_wandb
        if self.use_wandb:
            import wandb
            self.wandb = wandb
            self.wandb.init(title)
        self.layers = layers
        self.weights_and_biases = []
        self.activation_functions = []
        self.bc_func = bc_func
        self.key = key
        self.initialize_layers()
        self.w_b_previous = deepcopy(self.weights_and_biases)

    def initialize_layers(self):
        """
        This function creates the correctly sized weights and biases.

        :return: None
        """
        for i in range(1, len(self.layers)):
            _, self.key = random.split(self.key)
            f_activation = self.layers[i].activation

            if f_activation in (jax.nn.tanh, jax.nn.sigmoid):
                correction_factor = jax.numpy.sqrt(1 / self.layers[i - 1].n_neurons)
            elif f_activation in (jax.nn.relu, jax.nn.relu6, jax.nn.leaky_relu):
                correction_factor = jax.numpy.sqrt(2 / self.layers[i - 1].n_neurons)
            else:
                warnings.warn("Unexpected activation function, using sub-optimal initialization")
                correction_factor = jax.numpy.sqrt(2 / (self.layers[i - 1].n_neurons + self.layers[i].n_neurons))

            w = random.normal(key=self.key,
                              shape=(self.layers[i - 1].n_neurons, self.layers[i].n_neurons)) * correction_factor
            b = random.normal(key=self.key,
                              shape=(self.layers[i].n_neurons, 1)) * correction_factor

            self.weights_and_biases.append([w, b])
            self.activation_functions.append(f_activation)

        # Let's just quickly count the number of parameters.
        n_parameters = 0

        for w, b in self.weights_and_biases:
            n_parameters += w.size + b.size

        if self.use_wandb:
            self.wandb.n_parameters = n_parameters

    def _forward(self, weights_and_biases, activation_functions, x):
        x_i = x
        for i in range(len(self.layers) - 1):
            w = weights_and_biases[i][0]
            b = weights_and_biases[i][1]
            f_activation = activation_functions[i]

            x_i = f_activation(jnp.dot(x_i, w) + b.T)

        return self.bc_func(x=x, y=x_i)

    def forward(self, x):
        return self._forward(weights_and_biases=self.weights_and_biases,
                             activation_functions=self.activation_functions,
                             x=x)
