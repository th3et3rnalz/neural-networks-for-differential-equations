from helpers import LinearLayer, GeneralNeuralNetwork, update_w_b, norm_grad_f_squared
from typing import List
import jax
import matplotlib.pyplot as plt
from copy import deepcopy
import numpy as np


class ODESolver(GeneralNeuralNetwork):
    def __init__(self, layers: List[LinearLayer], bc_func: callable,
                 title, ode_func: callable, order: int = 1, use_wandb=True):
        super().__init__(layers=layers, bc_func=bc_func, title=title, use_wandb=use_wandb)
        if order not in (0, 1, 2):
            raise NotImplementedError
        self.order = order
        self.y_x = None
        self.y_xx = None
        self.ode_func = ode_func
        self.value_and_grad_network = None
        self.momentum = 0.8
        self.bA_beta = 0.1
        self.alpha_initial = 0.1

        if self.use_wandb:
            self.wandb.config.momentum = self.momentum
            self.wandb.config.bA_beta = self.bA_beta
            self.wandb.config.alpha_0 = self.alpha_initial

    def learn(self, x):
        if self.order >= 1 and self.y_x is None:
            self.y_x = jax.vmap(fun=jax.jacobian(self._forward, 2), in_axes=(None, None, 0))

        if self.order == 2 and self.y_xx is None:
            self.y_xx = jax.vmap(fun=jax.hessian(self._forward, 2), in_axes=(None, None, 0))

        def loss(w_b, x):
            # This function is a bit messy, but allows this system to solve many 0th order, 1st order and 2nd order ODEs
            x = x.reshape(len(x), 1)
            y = self._forward(weights_and_biases=w_b,
                              activation_functions=self.activation_functions,
                              x=x)
            if self.order == 0:
                return jax.numpy.mean(self.ode_func(y=y, x=x) ** 2)

            y_x = self.y_x(w_b, self.activation_functions, x)
            y_x = y_x.reshape(len(y_x), 1)

            if self.order == 1:
                return jax.numpy.mean(self.ode_func(y_x=y_x, y=y, x=x) ** 2)

            y_xx = self.y_xx(w_b, self.activation_functions, x)
            y_xx = y_xx.reshape(len(y_xx), 1)
            if self.order == 2:
                return jax.numpy.mean(self.ode_func(y_xx=y_xx, y_x=y_x, y=y, x=x)**2)
            raise NotImplementedError

        if self.value_and_grad_network is None:
            self.value_and_grad_network = jax.value_and_grad(loss, 0)

        w_b = deepcopy(self.weights_and_biases)
        for i in range(len(w_b)):
            w_b[i][0] += self.momentum*(w_b[i][0] - self.w_b_previous[i][0])
            w_b[i][1] += self.momentum*(w_b[i][1] - self.w_b_previous[i][1])

        loss_value, grad_values = self.value_and_grad_network(w_b, x)

        # == Trust region method implementation. ==
        alpha_i = self.alpha_initial

        w_b_i = update_w_b(w_b=deepcopy(self.weights_and_biases), step_size=alpha_i,
                           g_val=grad_values, w_b_previous=self.w_b_previous, beta=self.momentum)
        loss_i = loss(w_b_i, x)
        norm_grad_f_sq = norm_grad_f_squared(g_val=grad_values)

        while loss_i > loss_value - alpha_i * self.bA_beta * norm_grad_f_sq:
            alpha_i *= 0.5
            w_b_i = update_w_b(w_b=deepcopy(self.weights_and_biases), step_size=alpha_i,
                               g_val=grad_values, w_b_previous=self.w_b_previous, beta=self.momentum)
            loss_i = loss(w_b_i, x)

        # The backtracking converged onto a feasible alpha_i, so let's update our weights and biases.
        self.w_b_previous = self.weights_and_biases
        self.weights_and_biases = w_b_i

        if self.use_wandb:
            self.wandb.log({"loss": loss_i, "step_size": alpha_i})

        return loss_i, alpha_i


if __name__ == "__main__":
    jnp = jax.numpy

    def bc_func(x, y):
        return y*x

    def ode_func(y_x, y, x):
        return y_x - 2*jnp.pi*jnp.cos(2*np.pi*x)


    layers = [LinearLayer(1, activation=jax.nn.sigmoid),
              LinearLayer(50, activation=jax.nn.sigmoid),
              LinearLayer(50, activation=jax.nn.sigmoid),
              LinearLayer(1)]

    # print("before")
    nn = ODESolver(layers=layers, bc_func=bc_func, ode_func=ode_func, order=1, title="ODESolving", use_wandb=False)
    # print("after")

    x_train = jax.numpy.linspace(0, 2, 1024).reshape(1024, 1)
    # nn.wandb.config.n_points = len(x_train)

    loss_arr = []
    alpha_arr = []
    intermediate_steps = [jnp.asarray(nn.forward(x_train))[:, 0]]

    n_iterations = 4000
    # nn.wandb.config.n_iterations = n_iterations

    for i in range(n_iterations):
        loss_i, alpha_i = nn.learn(x_train)
        loss_arr.append(loss_i)
        alpha_arr.append(alpha_i)

        if i % 10 == 0:
            print(i, loss_arr[-1])
            intermediate_steps.append(jnp.asarray(nn.forward(x_train))[:, 0])

    np.savetxt('../basics/data.txt', jnp.array(intermediate_steps))

    plt.subplot(2, 1, 1)
    plt.plot(x_train, nn.forward(x_train), label="Neural network")
    plt.plot(x_train, jnp.sin(2*jnp.pi*x_train), label="Exact")
    plt.grid()
    plt.legend()
    plt.title("Final predictions")

    plt.subplot(2, 2, 3)
    plt.title("Loss progression")
    plt.semilogy(loss_arr)
    plt.xlabel("Epochs")

    plt.subplot(2, 2, 4)
    plt.title("Step size progression")
    plt.semilogy(alpha_arr)
    plt.xlabel("Epochs")

    plt.tight_layout()
    plt.show()






