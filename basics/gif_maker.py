"""
File for creating gif's from arrays.
Based on https://matplotlib.org/stable/gallery/animation/double_pendulum.html
Joe Verbist, Feb 2022.
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

data = np.loadtxt("data.txt")

fig = plt.figure()
ax = plt.axes(ylim=(-1.5, 1.5))
x = np.arange(0, len(data[0]))
ln, = ax.plot(x, data[0])
time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)


def init():
    ln.set_data([], [])
    # ln.grid()
    # ln.xlabel("X [-]")
    # ln.ylabel("Temperature [-]")
    return ln,


def update(i):
    ln.set_data(x, data[i])
    time_text.set_text(f"Epoch {i}/{len(data)}")
    return ln,


ani = FuncAnimation(fig, update, frames=len(data), init_func=init, blit=True)
ani.save('myAnimation.gif', writer='imagemagick', fps=10)
