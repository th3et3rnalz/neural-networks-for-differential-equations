from jax import numpy as jnp, grad, hessian, random, jit, value_and_grad
import matplotlib.pyplot as plt
from typing import List
from random import choice
import jax


class LinearLayer:
    def __init__(self, number_of_neurons, activation=lambda x: x):
        """
        A simple linear layer, purely create for easy network initialization.

        :param number_of_neurons: The number of neurons in this specific layer
        :param activation: Activation function, default to linear.
        """
        self.n_neurons = number_of_neurons
        self.activation = activation


class Network:
    def __init__(self, layer_list: List[LinearLayer], seed=random.PRNGKey(1), loss="LSE"):
        self.loss = loss
        self.layers = layer_list
        self.key = seed
        self.weights_and_biases = []
        self.activation_functions = []
        self.initialize_layers()
        self.a = 1
        self.b = 1

        self.y_xx = None
        self.value_and_grad_network = None

    def initialize_layers(self):
        """
        This function creates the correctly sized weights and biases.

        :return: None
        """
        for i in range(1, len(self.layers)):
            w = random.uniform(key=self.key,
                               shape=(self.layers[i - 1].n_neurons, self.layers[i].n_neurons),
                               minval=-1,
                               maxval=1)
            b = random.uniform(key=self.key,
                               shape=(self.layers[i].n_neurons, 1),
                               minval=-1,
                               maxval=1)
            f_activation = self.layers[i].activation
            self.weights_and_biases.append([w, b])
            self.activation_functions.append(f_activation)

    def _forward(self, weights_and_biases, activation_functions, x):
        x_i = x
        for i in range(len(self.layers) - 1):
            w = weights_and_biases[i][0]
            b = weights_and_biases[i][1]
            f_activation = activation_functions[i]

            x_i = f_activation(jnp.dot(x_i, w) + b.T)

        return self.a + x * (self.b - self.a) + x * (1 - x) * x_i

    def forward(self, x):
        return self._forward(weights_and_biases=self.weights_and_biases,
                             activation_functions=self.activation_functions,
                             x=x)

    def learn(self, x, method="batches", step_size=0.01):
        if method != "batches":
            return NotImplementedError

        if self.y_xx is None:
            self.y_xx = hessian(self._forward, 2)

        def loss(w_b, x):
            y_xx_sum = 0
            for i in range(len(x)):
                x_i = x[i]
                y_xx_i = self.y_xx(w_b, self.activation_functions, x_i)
                y_xx_sum += (y_xx_i + 2)**2/2

            return jnp.reshape(y_xx_sum, ())

        if self.value_and_grad_network is None:
            self.value_and_grad_network = value_and_grad(loss, 0)

        loss_value, grad_values = self.value_and_grad_network(self.weights_and_biases, x)
        for i in range(len(grad_values)):
            self.weights_and_biases[i][0] += -step_size * grad_values[i][0]
            self.weights_and_biases[i][1] += -step_size * grad_values[i][1]

        return loss_value / len(x)


# == SETTING UP THE NETWORK ==
key = random.PRNGKey(1)
layers = [LinearLayer(1, activation=jax.nn.sigmoid),
          LinearLayer(4, activation=jax.nn.sigmoid),
          LinearLayer(4, activation=jax.nn.sigmoid),
          LinearLayer(1)]

network = Network(layer_list=layers)

x = jnp.linspace(0, 1, 100)
x = random.shuffle(key=key, x=x)
x = x.reshape((len(x), 1))

loss_lst = []
for i in range(40):
    x_sub = random.choice(key=key, a=x, shape=(40,))
    step_size = 0.004
    loss = network.learn(x=x_sub, step_size=step_size)
    print(loss, step_size)
    loss_lst.append(loss)
    if loss < 1e-4:
        break

plt.subplot(2, 1, 1)
plt.title("Loss behaviour")
plt.semilogy(loss_lst)
plt.xlabel("Epoch [-]")

plt.subplot(2, 1, 2)
x = jnp.linspace(0, 1, 100)
y = [network.forward(x_i)[0][0] for x_i in x]
plt.title(f"Prediction (final loss {round(loss_lst[-1],6)}")
plt.ylabel("y")
plt.xlabel("x")
plt.plot(x, y)
plt.tight_layout()
plt.savefig("shape.pdf")
plt.show()
