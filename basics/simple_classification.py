import matplotlib.pyplot as plt
import csv
import numpy as np


def stable_sigmoid(x):
    sig = np.where(x < 0, np.exp(x)/(1 + np.exp(x)), 1/(1 + np.exp(-x)))
    return sig


class Network:
    def __init__(self, input_size, layer_1_size):
        self.w_1 = np.random.random(size=(layer_1_size, input_size))
        self.b_1 = np.random.random(size=(layer_1_size, 1))
        self.w_2 = np.random.random(size=(1, layer_1_size))
        self.b_2 = np.random.random(size=(1, 1))

        self.input_size = input_size
        self.activation = stable_sigmoid
        self.d_activation = lambda x: stable_sigmoid(x)*(1-stable_sigmoid(x))
        self.loss_func = lambda y, y_hat: - (y*np.log(y_hat) + (1-y)*np.log(1-y_hat))

    def forward(self, x_1, return_all=False):
        if len(x_1) != self.input_size:
            raise Exception(f"Wrong input size: received {len(x_1)}, but expected {self.input_size}")

        x_2 = self.activation(np.matmul(self.w_1, x_1) + self.b_1)
        y = self.activation(np.matmul(self.w_2, x_2) + self.b_2)
        if return_all:
            return x_2, y
        return y

    def gradient_at_(self, x_1):
        if len(x_1) != self.input_size:
            raise Exception(f"Wrong input size: received {len(x_1)}, but expected {self.input_size}")
        x_2, y = self.forward(x_1=x_1, return_all=True)
        d_sigma_alpha_2 = self.d_activation(np.matmul(self.w_2, x_2) + self.b_2)
        d_sigma_alpha_1 = self.d_activation(np.matmul(self.w_1, x_1) + self.b_1)

        dy_db_2 = d_sigma_alpha_2  # scalar
        dy_dw_2 = d_sigma_alpha_2 * x_2.T  # input_size x 1 vector

        dy_db_1 = d_sigma_alpha_2 * self.w_2.T * d_sigma_alpha_1  # input_size x 1 vector
        dy_dw_1 = d_sigma_alpha_2 * self.w_2.T * d_sigma_alpha_1 * x_1.T  # layer_1_size x input_vector

        return dy_db_2, dy_dw_2, dy_db_1, dy_dw_1, y, x_2

    def learn(self, data_set, step_size=0.1):
        loss = 0

        for x, y in data_set:
            x = np.array(x).reshape(len(x), 1)

            dy_db_2, dy_dw_2, dy_db_1, dy_dw_1, y_hat, x_2 = self.gradient_at_(x_1=x)
            dl_dy = y_hat - y

            d2 = -y/y_hat + (1-y)/(1-y_hat)

            self.b_1 -= step_size*d2*self.d_activation(x_2)
            self.w_1 -= step_size*dl_dy*dy_dw_1

            self.b_2 -= step_size*dl_dy*dy_db_2
            self.w_2 -= step_size*dl_dy*dy_dw_2

            loss += self.loss_func(y=y, y_hat=y_hat)/len(data_set)

        return loss


# ==== GETTING THE DATA ====
def list_of_strings_to_list_of_floats(lst):
    return [float(el) for el in lst]


with open("simple_data.csv", 'r') as f:
    csv_file = csv.reader(f)
    # Just throw away the very first entry.
    data = [[line[:-1], line[-1]] for line in csv_file][1:]
    # next, throw away the last third of the data:
    data = [el for el in data if int(el[1]) != 2]
    # Next convert all strings to float or int:
    data = [[list_of_strings_to_list_of_floats(el[0]), int(el[1])] for el in data]


# ==== SETTING UP THE NET ====
net = Network(input_size=len(data[0][0]), layer_1_size=10)
# quick trial run of the data:
net.forward(x_1=data[0][0])

# ==== LEARNING IT ALL ====
n_epochs = 500
losses = []

print("Size of the dataset ", len(data))

for _ in range(n_epochs):
    total_loss = net.learn(data_set=data, step_size=0.01)
    losses.append(float(total_loss))
    print(total_loss)

plt.plot(losses)
plt.show()
