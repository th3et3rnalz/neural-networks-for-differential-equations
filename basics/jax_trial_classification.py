from jax_trial import Network, LinearLayer
from jax import numpy as jnp
import jax
import matplotlib.pyplot as plt
import csv


# ==== GETTING THE DATA ====
def list_of_strings_to_list_of_floats(lst):
    return [float(el) for el in lst]


with open("simple_data.csv", 'r') as f:
    csv_file = csv.reader(f)
    # Just throw away the very first entry.
    data = [[line[:-1], line[-1]] for line in csv_file][1:]
    # next, throw away the last third of the data:
    data = [el for el in data if int(el[1]) != 2]
    # Next convert all strings to float or int:
    data = [[list_of_strings_to_list_of_floats(el[0]), int(el[1])] for el in data]

# ==== SETTING UP THE NETWORK ====

layers = [LinearLayer(4, activation=jax.nn.sigmoid),
          LinearLayer(15, activation=jax.nn.sigmoid),
          LinearLayer(1, activation=jax.nn.sigmoid)]

network = Network(layer_list=layers, loss="Logistic")

x, y = zip(*data)
x = jnp.array(x)
y = jnp.array(y)

loss_lst = []
for _ in range(100):
    l = network.learn(x=x, y=y, step_size=0.01)
    print(l)
    loss_lst.append(l)

plt.plot(loss_lst)
plt.show()
