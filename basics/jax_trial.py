from jax import numpy as jnp, grad, random
from typing import List
from random import randint
import matplotlib.pyplot as plt
import jax


class LinearLayer:
    def __init__(self, number_of_neurons, activation=lambda x: x):
        """
        A simple linear layer, purely create for easy network initialization.

        :param number_of_neurons: The number of neurons in this specific layer
        :param activation: Activation function, default to linear.
        """
        self.n_neurons = number_of_neurons
        self.activation = activation


class Network:
    def __init__(self, layer_list: List[LinearLayer], seed=jax.random.PRNGKey(1), loss="LSE"):
        self.loss = loss
        self.layers = layer_list
        self.key = seed
        self.weights_and_biases = []
        self.activation_functions = []
        self.initialize_layers()

    def initialize_layers(self):
        """
        This function creates the correctly sized weights and biases.

        :return: None
        """
        for i in range(1, len(self.layers)):
            w = jax.random.uniform(key=self.key,
                                   shape=(self.layers[i - 1].n_neurons, self.layers[i].n_neurons),
                                   minval=-1,
                                   maxval=1)
            b = jax.random.uniform(key=self.key,
                                   shape=(self.layers[i].n_neurons, 1),
                                   minval=-1,
                                   maxval=1)
            f_activation = self.layers[i].activation
            self.weights_and_biases.append([w, b])
            self.activation_functions.append(f_activation)

    def _forward(self, weights_and_biases, activation_functions, x):
        x_i = x
        for i in range(len(self.layers) - 1):
            w = weights_and_biases[i][0]
            b = weights_and_biases[i][1]
            f_activation = activation_functions[i]

            x_i = f_activation(jnp.dot(x_i, w) + b.T)

        return x_i

    def forward(self, x):
        return self._forward(weights_and_biases=self.weights_and_biases,
                             activation_functions=self.activation_functions,
                             x=x)

    def learn(self, x, y, method="batches", step_size=0.01):
        if method != "batches":
            return NotImplementedError

        if self.loss == "LSE":
            def loss(w_b, x):
                y_hat = self._forward(w_b, self.activation_functions, x)
                return jnp.sum((y_hat - y) ** 2)
        elif self.loss == "Logistic":
            def loss(w_b, x):
                y_hat = self._forward(w_b, self.activation_functions, x)
                return -jnp.sum(y*jnp.log(y_hat) + (1 - y)*jnp.log(1 - y_hat)) / len(x)
        else:
            raise NotImplementedError

        loss_value = loss(self.weights_and_biases, x)

        grad_network = grad(loss)
        gradding = grad_network(self.weights_and_biases, x)
        for i in range(len(gradding)):
            self.weights_and_biases[i][0] += -step_size * gradding[i][0]
            self.weights_and_biases[i][1] += -step_size * gradding[i][1]

        return loss_value / len(x)


if __name__ == "__main__":
    layers = [LinearLayer(1, activation=jax.nn.sigmoid),
              LinearLayer(10, activation=jax.nn.sigmoid),
              LinearLayer(1, activation=jax.nn.sigmoid)]

    key = jax.random.PRNGKey(randint(0, 100))

    s_ode_net = Network(layer_list=layers, loss="Logistic")

    key, x_key, y_key = random.split(key, 3)

    x = jax.random.uniform(key=x_key, shape=(10, 1))
    y = jax.random.uniform(key=y_key, shape=(10, 1))
    print(x, y)

    loss_lst = []
    for _ in range(100):
        loss = s_ode_net.learn(x=x, y=y, step_size=0.001)
        loss_lst.append(loss)

    plt.plot(loss_lst)
    plt.show()
