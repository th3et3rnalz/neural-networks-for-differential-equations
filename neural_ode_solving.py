import os
# The below environment variable is needed for my machine to get around an Out Of Memory (OOM) issue.
# Feel free to remove.
os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

import matplotlib.pyplot as plt
import numpy as onp
from jax.nn import sigmoid
import jax.numpy as np
from jax import vmap, hessian, jit, random, value_and_grad, jacobian
from random import randint
import optax


# ==================================================================================================================
# ===================== PROBLEM SPECIFIC SETUP ===========================
# ==================================================================================================================
def ode_func(y_xx, y_x, y, x):
    return y_xx + y_x/5 + y + np.exp(-x/5)*np.cos(x)/5


def bc_func(x, y):
    return x + x*x*y

# def ode_func(y_xx, y_x, y, x):
#     return y_x + y/5 - np.exp(-x/5)*np.cos(x)
#
#
# def bc_func(x, y):
#     return x*y


# ==================================================================================================================
# ===================== GENERAL NEURAL NETWORK FUNCTIONS ===========================
# ==================================================================================================================
def forward_pass(params, x_0):
    x = x_0.reshape(1, -1)

    for w, b in params[: -1]:
        x = sigmoid(np.dot(w, x) + b)

    final_w, final_b = params[-1]
    y_hat = np.dot(final_w, x) + final_b
    return bc_func(x_0, y_hat)


batch_forward = vmap(forward_pass, in_axes=(None, 0), out_axes=0)


def loss(w_b, x):
    y = batch_forward(w_b, x).reshape(1, -1)
    y_x = vmap(fun=jacobian(batch_forward, 1), in_axes=(None, 0))(w_b, x.T).reshape(1, -1)
    y_xx = vmap(fun=hessian(batch_forward, 1), in_axes=(None, 0))(w_b, x.T).reshape(1, -1)

    return np.mean(np.square(ode_func(y_xx=y_xx, y_x=y_x, y=y, x=x)))


def initialize_nn_wb(sizes, key):
    keys = random.split(key, len(sizes))

    def initialize_layer(m, n, key, scale=None):
        if scale is None:
            # scale = np.sqrt(2 / n)*0.1
            # print(scale)
            scale = 1e-2
        w_key, b_key = random.split(key)
        weight_matrix = scale * random.normal(w_key, (n, m))
        bias_vector = scale * random.normal(b_key, (n, 1))

        return weight_matrix, bias_vector

    params = []
    for i in range(len(sizes) - 1):
        m = sizes[i]
        n = sizes[i + 1]
        params.append(initialize_layer(m=m, n=n, key=keys[i]))

    return params


@jit  # Adam optimizer
def update(w_b, x, opt_state):
    """Compute the gradient for a batch and update the parameters"""
    loss_v, grads = value_and_grad(loss)(w_b, x)
    updates, opt_state = optimizer.update(grads, opt_state)
    params = optax.apply_updates(w_b, updates)
    return params, opt_state, loss_v


# ==================================================================================================================
# ===================== GETTING GOING WITH THE INTERESTING STUFF ===========================
# ==================================================================================================================
layers = [1, 10, 1]

key = random.PRNGKey(randint(0, 2**16-1))
w_b = initialize_nn_wb(sizes=layers, key=key)

optimizer = optax.adam(0.01)
opt_state = optimizer.init(w_b)

x = np.linspace(0, 2, 4096, dtype=np.float64).reshape(1, -1)

for i in range(10000):
    w_b, opt_state, loss = update(w_b=w_b, x=x, opt_state=opt_state)
    if i % 50 == 0:
        print(loss)

y = batch_forward(w_b, x)
y = onp.array(y).reshape(-1)
x = x.reshape(-1)
plt.plot(x, y, label="Computed")
plt.plot(x, onp.exp(-x/5)*onp.sin(x), label="True")
plt.legend()
plt.show()
