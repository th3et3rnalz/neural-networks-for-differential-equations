from first_order_solving import train_neural_network, np
from optax import adam


def train_and_return(data):
    learning_rate, scaling, n_x_points, layers = data
    x = np.linspace(0, 6, n_x_points, dtype=np.float32).reshape(1, -1)
    optim = adam(learning_rate=learning_rate)
    loss, w_b = train_neural_network(optimizer=optim,
                                     initialization_scale=scaling,
                                     layers=layers,
                                     x=x,
                                     epochs=int(1000 * 1000 / n_x_points),
                                     debug=False)

    # some average of the last loss values to take into account whether the algorithm was able to converge or not
    print(sum(loss[-100:]) / 100, end="")
    return sum(loss[-100:]) / 100


if __name__ == "__main__":
    loss, w_b = train_and_return((0.01, 0.01, 10, [1, 30, 30, 30, 1]))
    print(loss[-1])
