from first_order_solving import train_neural_network, np, forward_pass, initialize_nn_wb, random, randint
from optax import adam
import matplotlib.pyplot as plt


def train_and_return(n_x_points, return_initial=False):
    x = np.linspace(0, 1, n_x_points, dtype=np.float32).reshape(1, -1)
    optim = adam(learning_rate=0.01)
    data = train_neural_network(optimizer=optim,
                                     initialization_scale=0.01,
                                     layers=[1, 20, 1],
                                     x=x,
                                     epochs=int(100 * 1000 / n_x_points),
                                     debug=False,
                                     return_initial=return_initial)
    return data


x_large = np.linspace(0, 1, 1024)
y_true = lambda x : np.exp(-x / 5) * np.sin(x)


loss, w_b, w_b_initial = train_and_return(n_x_points=2, return_initial=True)

y = forward_pass(params=w_b_initial, x_0=x_large.reshape(1, -1)).reshape(-1)
plt.semilogy(x_large, np.abs(y_true(x_large) - y), label="untrained")

y = forward_pass(params=w_b, x_0=x_large.reshape(1, -1)).reshape(-1)
plt.semilogy(x_large, np.abs(y_true(x_large) - y), label="2")


for n_points in range(4, 11, 2):
    loss, w_b = train_and_return(n_x_points=n_points)

    y = forward_pass(params=w_b, x_0=x_large.reshape(1, -1)).reshape(-1)
    plt.semilogy(x_large, np.abs(y_true(x_large) - y), label=str(n_points))
    print(".")

plt.legend()
plt.show()

