from helpers import train_and_return
import numpy as np
import statistics
import pandas
import multiprocessing


ms_arr = []
lr_arr = []
nx_arr = []
sc_arr = []
loss_arr = []
loss_std_arr = []

i = 0
pool = multiprocessing.Pool(processes=10)

for middle_size in [[20], [10, 10], [7, 7, 6], [5, 5, 5, 5]]:
    for learning_rate in [0.01, 0.001, 0.0001]:
        for n_x_points in [10, 100, 1000]:
            for scaling in [0.1, 0.01, 0.001]:
                layers = [1, *middle_size, 1]
                n_repeats = 10

                res = pool.map(train_and_return, [[learning_rate, scaling, n_x_points, layers]] * n_repeats)

                loss_runs = [float(x) for x in res]
                ms_arr.append(middle_size)
                lr_arr.append(learning_rate)
                nx_arr.append(n_x_points)
                sc_arr.append(scaling)

                loss_arr.append(statistics.mean(loss_runs))
                loss_std_arr.append(statistics.variance(loss_runs))

                print(f"\n{i} - {statistics.mean(loss_runs):.1e} +/- {statistics.stdev(loss_runs):.1e}")
                i += 1

i = np.argmin(np.array(loss_arr))
print("Optimal:", i, ms_arr[i])

df = pandas.DataFrame({"learning_rate": lr_arr,
                       "Layers": ms_arr,
                       "Scaling": sc_arr,
                       "Points": nx_arr,
                       "Loss": loss_arr,
                       "Loss_std": loss_std_arr})

df.to_pickle("grid_search.pkl")
