from jax import vmap, hessian, jit, random, value_and_grad, jacobian
import matplotlib.pyplot as plt
from random import randint
from jax.nn import sigmoid
import jax.numpy as np
import numpy as onp
import optax
import os
# The below environment variable is needed for my machine to get around an Out Of Memory (OOM) issue.
# Feel free to remove.
os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'


# ==================================================================================================================
# ===================== PROBLEM SPECIFIC SETUP ===========================
# ==================================================================================================================
def pde_func(p_yy, p_xx, y, x):
    return p_yy + p_xx - np.exp(-x) * (x - 2 + y * y * y + 6 * y)


# def pde_func(p_yy, p_xx, y, x):
#     p1 = (-4*27*x/5 - 2/5 + 2*9)*np.cos(9*x*x + y)
#     p2 = (-15/25-4*81*x*x)*np.sin(9*x*x + y)
#     return p_yy + p_xx - np.exp(-(3*x+y)/5)*(p1 - p2)


def a(x, y):
    y_3 = np.power(y, 3)
    enx = np.exp(-x)
    e1 = np.exp(-1)
    return (1-x)*y_3 + x*(1+y_3)*e1 + (1-y)*x*(enx - e1) + y*((1+x)*enx - (1-x-2*x*e1))


def bc_func(x, y, p):
    return a(x, y) + x*(1-x)*(1-y)*y*p


# ==================================================================================================================
# ===================== GENERAL NEURAL NETWORK FUNCTIONS ===========================
# ==================================================================================================================
def forward_pass(params, x_vec):
    assert x_vec.shape[0] == 2
    x = x_vec

    for w, b in params[: -1]:
        x = sigmoid(np.dot(w, x) + b)

    final_w, final_b = params[-1]
    o_hat = np.dot(final_w, x) + final_b

    return bc_func(x=x_vec[0, :], y=x_vec[1, :], p=o_hat)


batch_forward = vmap(forward_pass, in_axes=(None, 1), out_axes=0)


def loss(w_b, x):
    # p = batch_forward(w_b, x[..., np.newaxis]).reshape(1, -1)
    # p_d = vmap(fun=jacobian(forward_pass, 1), in_axes=(None, 1))(w_b, x[..., np.newaxis])[:, 0, 0, :, 0]
    # p_x = p_d[:, 0].reshape(-1)
    # p_y = p_d[:, 1].reshape(-1)

    p_dd = vmap(fun=hessian(forward_pass, 1), in_axes=(None, 1))(w_b, x[..., np.newaxis])[:, 0, 0, :, 0, :, 0]
    p_xx = p_dd[:, 0, 0]
    p_yy = p_dd[:, 1, 1]

    return np.mean(np.square(pde_func(p_xx=p_xx, p_yy=p_yy, y=x[1, :], x=x[0, :])))


def initialize_nn_wb(sizes, key):
    keys = random.split(key, len(sizes))

    def initialize_layer(m, n, key, scale=None):
        if scale is None:
            scale = 1e-2
        w_key, b_key = random.split(key)
        weight_matrix = scale * random.normal(w_key, (n, m))
        bias_vector = scale * random.normal(b_key, (n, 1))

        return weight_matrix, bias_vector

    params = []
    for i in range(len(sizes) - 1):
        m = sizes[i]
        n = sizes[i + 1]
        params.append(initialize_layer(m=m, n=n, key=keys[i]))

    return params


@jit  # Adam optimizer
def update(w_b, x, opt_state):
    """Compute the gradient for a batch and update the parameters"""
    loss_v, grads = value_and_grad(loss)(w_b, x)
    updates, opt_state = optimizer.update(grads, opt_state)
    params = optax.apply_updates(w_b, updates)
    return params, opt_state, loss_v


# ==================================================================================================================
# ===================== GETTING GOING WITH THE INTERESTING STUFF ===========================
# ==================================================================================================================

def train_neural_network(optimizer, epochs, x, layers, debug=False):
    key = random.PRNGKey(randint(0, 2**16-1))
    w_b = initialize_nn_wb(sizes=layers, key=key)
    loss_history = []

    opt_state = optimizer.init(w_b)

    for i in range(epochs):
        w_b, opt_state, loss_v = update(w_b=w_b, x=x, opt_state=opt_state)
        if debug and i % 100 == 0:
            print(loss_v)
        loss_history.append(loss_v)

    return loss_history, w_b


if __name__ == "__main__":
    dim = 10
    x = np.linspace(0, 1, dim+1, dtype=np.float32)
    y = np.linspace(0, 1, dim+1, dtype=np.float32)

    xx, yy = np.meshgrid(x, y)
    xx_flat = xx.reshape(-1)
    yy_flat = yy.reshape(-1)
    z = np.vstack([xx_flat, yy_flat])

    optimizer = optax.adam(0.01)
    l_h, w_b = train_neural_network(optimizer=optimizer,
                                    epochs=100*1000,
                                    x=z,
                                    layers=[2, 10, 1],
                                    debug=True)

    plt.loglog(l_h)
    plt.show()

    p = forward_pass(w_b, z)
    p = p.reshape(x.shape[0], y.shape[0])

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(xx, yy, p, label="NN")
    # ax.plot_surface(xx, yy, np.exp(-(3*x+y)/5)*np.sin(9*xx*xx + yy), label='True')
    # plt.legend()
    plt.show()
