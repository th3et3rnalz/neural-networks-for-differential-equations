

def loss(w_b, x):
    phi = forward_pass(w_b, x)
    dphi_dx = jacobian(forward_pass)(w_b, x)
    dphi_dxx = hessian(forward_pass)(w_b, x)
    return ode_error_func(dphi_dxx, dphi_dx, phi, x)**2

def update(w_b, x):
    grad = grad(loss)(w_b, x)
    w_b = w_b - alpha * grad
    return w_b