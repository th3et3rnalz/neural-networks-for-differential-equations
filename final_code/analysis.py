import plotly.graph_objects as go
import pandas as pd
import numpy as np

df = pd.read_pickle("hyper.pkl")
df['Layers'] = [len(el) for el in list(df["Layers"])]

print(df)

# fig = go.Figure(data=
# go.Parcoords(
#     line=dict(color=df['Loss'],
#               colorscale=[[0, 'purple'], [0.5, 'lightseagreen'], [1, 'gold']],
#               showscale=True,
#               cmin=0,
#               cmax=df["Loss"].max()),
#     dimensions=list([
#         dict(range=[0, 0.01],
#              label="Learning Rate", values=df['learning_rate']),
#         dict(range=[1, 4],
#              label='Layers', values=df['Layers']),
#         dict(range=[0.0001, 0.1],
#              label='Scale', values=df['Scaling']),
#         dict(range=[10, 1000],
#              label="Points", values=df["Points"]),
#         dict(range=[0, df["Loss"].max()],
#              label="Loss", values=df["Loss"])]
#     )
# ))
#
# fig.update_yaxes(type="log")
# fig.show()


loss = df["Loss"].to_numpy()
print(df.loc[18])