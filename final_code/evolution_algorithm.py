import multiprocessing

from helpers import train_and_return
from evolutionary_algorithm import EvolutionaryAlgorithm as ea

# Declare objective function parameters and bounds
objective_parameters = [
    {'name': 'learning_rate',
     'bounds': [0, 0.01],
     'type': 'float'},
    {'name': 'scaling',
     'bounds': [0, 0.1],
     'type': 'float'},
    {'name': 'n_x_points',
     'bounds': ["32", "64", "128", "256"],
     'type': 'cat'},
    {'name': 'layers',
     'bounds': ["1", "2", "3", "4"],
     'type': 'cat'}]


pool = multiprocessing.Pool(processes=4)


def to_train(args):
    learning_rate = args["learning_rate"]
    scaling = args["scaling"]
    n_x_points = int(args["n_x_points"])
    layers = args["layers"]

    layers = {"1": [20],
              "2": [10, 10],
              "3": [7, 7, 6],
              "4": [5, 5, 5, 5]}[layers]
    layers = [1, *layers, 1]
    n_x_points = int(n_x_points)
    eps = 1e-4

    res = pool.map(train_and_return, [[learning_rate+eps, scaling+eps, n_x_points, layers]]*4)

    print(sum(res)/4)

    return sum(res)/4


# Create instance of EA object
evo_algo = ea(function=to_train,
              parameters=objective_parameters,
              function_timeout=200,
              algorithm_parameters={'max_num_iteration': None,
                                    'population_size': 100,
                                    'mutation_probability': 0.1,
                                    'elite_ratio': 0.05,
                                    'crossover_probability': 0.5,
                                    'parents_portion': 0.3,
                                    'crossover_type': 'uniform',
                                    'max_iteration_without_improv': None})  # seconds

# Run EA
evo_algo.run()

# Access best model parameters
print(evo_algo.best_parameters)
