import time
import os
from copy import deepcopy
# The below environment variable is needed for my machine to get around an Out Of Memory (OOM) issue.
# Feel free to remove.
os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

import matplotlib.pyplot as plt
import numpy as onp
from jax.nn import sigmoid
import jax.numpy as np
from jax import vmap, hessian, jit, random, value_and_grad, jacobian
from random import randint
import optax


# ==================================================================================================================
# ===================== PROBLEM SPECIFIC SETUP ===========================
# ==================================================================================================================
# ------------------ PROBLEM 3 ------------------
# def ode_func(y_xx, y_x, y, x):
#     return y_xx + y_x/5 + y + np.exp(-x/5)*np.cos(x)/5
#
#
# def bc_func(x, y):
#     return x*np.sin(1)*np.exp(-1/5) + x*(1-x)*y

# ------------------ PROBLEM 2 ------------------

def ode_func(y_xx, y_x, y, x):
    return y_x + y/5 - np.exp(-x/5)*np.cos(x)


def bc_func(x, y):
    return x*y

# ------------------ PROBLEM ME ------------------
#
# def ode_func(y_xx, y_x, y, x):
#     return y_xx + 4*np.pi*np.pi*np.sin(x*np.pi*4)
#
#
# def bc_func(x, y):
#     return y*x*(1-x)

# ------------------ PROBLEM ME 2------------------
# def ode_func(y_xx, y_x, y, x):
#     return y_x - y/4 - np.exp(x/4)*np.pi*np.cos(np.pi*x)
#
#
# def bc_func(x, y):
#     return x*y


# ==================================================================================================================
# ===================== GENERAL NEURAL NETWORK FUNCTIONS ===========================
# ==================================================================================================================
def forward_pass(params, x_0):
    x = x_0.reshape(1, -1)

    for w, b in params[: -1]:
        x = sigmoid(np.dot(w, x) + b)

    final_w, final_b = params[-1]
    y_hat = np.dot(final_w, x) + final_b
    return bc_func(x_0, y_hat)


batch_forward = vmap(forward_pass, in_axes=(None, 0), out_axes=0)


def loss(w_b, x):
    y = batch_forward(w_b, x).reshape(1, -1)
    y_x = vmap(fun=jacobian(batch_forward, 1), in_axes=(None, 0))(w_b, x.T).reshape(1, -1)
    y_xx = vmap(fun=hessian(batch_forward, 1), in_axes=(None, 0))(w_b, x.T).reshape(1, -1)

    return np.mean(np.square(ode_func(y_xx=y_xx, y_x=y_x, y=y, x=x)))


def initialize_nn_wb(sizes, key, scale):
    keys = random.split(key, len(sizes))

    def initialize_layer(m, n, key, scale):
        w_key, b_key = random.split(key)
        weight_matrix = scale * random.normal(w_key, (n, m))
        bias_vector = scale * random.normal(b_key, (n, 1))

        return weight_matrix, bias_vector

    params = []
    for i in range(len(sizes) - 1):
        m = sizes[i]
        n = sizes[i + 1]
        params.append(initialize_layer(m=m, n=n, key=keys[i], scale=scale))

    return params


# ==================================================================================================================
# ===================== GETTING GOING WITH THE INTERESTING STUFF ===========================
# ==================================================================================================================

def train_neural_network(optimizer, epochs, x, layers=[1, 10, 1], initialization_scale=1e-2, debug=False,
                         return_initial=False):
    @jit  # Adam optimizer
    def update(w_b, x, opt_state):
        """Compute the gradient for a batch and update the parameters"""
        loss_v, grads = value_and_grad(loss)(w_b, x)
        updates, opt_state = optimizer.update(grads, opt_state)
        params = optax.apply_updates(w_b, updates)
        return params, opt_state, loss_v

    key = random.PRNGKey(randint(0, 2**16-1))
    w_b_initial = initialize_nn_wb(sizes=layers, key=key, scale=initialization_scale)
    w_b = deepcopy(w_b_initial)
    loss_history = []

    opt_state = optimizer.init(w_b)

    # start_time = time.time()
    for i in range(epochs):
        w_b, opt_state, loss_v = update(w_b=w_b, x=x, opt_state=opt_state)
        if debug and i % 10000 == 0:
        #     plt.subplot(5, 2, int(i/10000)+1)
        #     x_l = np.linspace(0, 6, 1024, dtype=np.float32).reshape(1, -1)
        #     y_l = onp.array(batch_forward(w_b, x_l)).reshape(-1)
        #
        #     x_l = x_l.reshape(-1)
        #     plt.plot(x_l, np.sin(np.pi*x_l)*np.exp(x_l/4), label="True")
        #     plt.plot(x_l, y_l, label="NN")
        #     plt.title(f"Epoch={i}")
        #     plt.legend()
        #     # print(f"time_taken = {(time.time() - start_time)/100}")
        #     # start_time = time.time()
            print(loss_v)
        loss_history.append(loss_v)

    plt.show()

    if return_initial:
        return loss_history, w_b, w_b_initial

    return loss_history, w_b


if __name__ == "__main__":
    optimizer = optax.adam(0.001)
    n_points = 128
    x = np.linspace(0, 6, n_points, dtype=np.float32).reshape(1, -1)

    start_time = time.time()
    l_h, w_b = train_neural_network(optimizer=optimizer,
                                    epochs=int(10*1000*1000/n_points),
                                    x=x,
                                    layers=[1, 20, 1],
                                    debug=True,
                                    initialization_scale=1e-2)

    print(f"Total training time: {time.time() - start_time}")

    x = np.linspace(0, 6, 1024, dtype=np.float32).reshape(1, -1)
    y = forward_pass(w_b, x).reshape(-1)
    x = x.reshape(-1)

    plt.subplot(1, 3, 1)
    plt.title("Comparison of solution")
    plt.plot(x, y, label="NN")
    # y_true = np.exp(-x/5)*np.sin(x)
    # y_true = np.sin(x*np.pi*4)
    y_true = np.exp(x/4)*np.sin(np.pi*x)
    # plt.plot(x, np.exp(-x/5)*np.sin(x), label="True")
    plt.plot(x, y_true, label="True")
    plt.xlabel("x")
    plt.ylabel(r"$y(x)$")
    plt.legend()

    plt.subplot(1, 3, 2)
    plt.title("Error")
    plt.semilogy(x, np.abs(y - y_true))
    plt.xlabel("x")
    plt.ylabel(r"$y(x) - \hat{y}(x)$")

    plt.subplot(1, 3, 3)
    plt.title("Loss progression")
    x = np.linspace(0, 10*1000, len(l_h))
    plt.loglog(x, l_h)
    plt.xlabel("Epoch")
    plt.ylabel(r"$\mathcal{L}$")

    plt.tight_layout()
    plt.show()


