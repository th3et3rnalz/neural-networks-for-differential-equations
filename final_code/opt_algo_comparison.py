from first_order_solving import train_neural_network
import matplotlib.pyplot as plt
from optax import adam, sgd, adabelief
import numpy as np
import multiprocessing
import pandas as pd


def train_and_return(optim):
    if optim == "adam":
        optim = adam(learning_rate=0.001)
    elif optim == "adam2":
        optim = adam(learning_rate=0.0001)
    elif optim == "sgd":
        optim = sgd(learning_rate=0.001)
    elif optim == "sgd2":
        optim = sgd(learning_rate=0.0001)
    elif optim == "adabelief":
        optim = adabelief(learning_rate=0.001)

    n_x_points = 10
    layers = [1, 10, 10, 1]
    x = np.linspace(0, 2, n_x_points, dtype=np.float32).reshape(1, -1)
    scaling = 1e-2

    loss, w_b = train_neural_network(optimizer=optim,
                                     initialization_scale=scaling,
                                     layers=layers,
                                     x=x,
                                     epochs=int(1000 * 1000 / n_x_points),
                                     debug=False)

    return np.array(loss)


data = {}
pool = multiprocessing.Pool(processes=10)

res = pool.map(train_and_return, ["adam"]*10)
loss_a = sum(res)/10
plt.loglog(loss_a, label=r"Adam $\alpha=1e-3$")
data['adam 3'] = loss_a
print(".")

res = pool.map(train_and_return, ["adam2"]*10)
loss_a = sum(res)/10
plt.loglog(loss_a, label="Adam $\alpha=1e-4")
data['adam 4'] = loss_a
print(".")

res = pool.map(train_and_return, ["sgd"]*10)
loss_sgd = sum(res)/10
plt.loglog(loss_sgd, label="GD $\alpha=1e-3")
data['GD 3'] = loss_sgd
print(".")

res = pool.map(train_and_return, ["sgd2"]*10)
loss_sgd = sum(res)/10
plt.loglog(loss_sgd, label="GD $\alpha=1e-4$")
data['GD 4'] = loss_sgd
print(".")

res = pool.map(train_and_return, ["adabelief"]*10)
loss_sgd = sum(res)/10
plt.loglog(loss_sgd, label="AdaBelief")
data['Adabelief 3'] = loss_sgd
print(".")

res = pool.map(train_and_return, ["adabelief2"]*10)
loss_sgd = sum(res)/10
plt.loglog(loss_sgd, label="AdaBelief $\alpha=1e-4$")
data['Adabelief 4'] = loss_sgd

plt.legend()
plt.show()

df = pd.DataFrame(data)
df.to_pickle("opt_algo_comparison.pkl")
