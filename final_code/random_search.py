from helpers import train_and_return
import sobol
import multiprocessing
import statistics
import pandas


ms_arr = []
lr_arr = []
nx_arr = []
sc_arr = []
loss_arr = []
loss_std_arr = []


pool = multiprocessing.Pool(processes=10)


for layers in [[20], [10, 10], [7, 7, 6], [5, 5, 5, 5]]:
    c_r = sobol.sample(dimension=3, n_points=10)

    for i in range(10):
        c_alpha = c_r[i, 0]*2 - 3
        c_np = c_r[i, 1]*2 + 1
        c_eps = c_r[i, 2]*2 - 3

        print(i, end="")
        learning_rate = 10**c_alpha
        scaling = 10**c_eps
        n_x_points = int(10**c_np)
        n_repeats = 4

        res = pool.map(train_and_return, [[learning_rate, scaling, n_x_points, layers]] * n_repeats)

        loss_runs = [float(x) for x in res]
        ms_arr.append(layers)
        lr_arr.append(learning_rate)
        nx_arr.append(n_x_points)
        sc_arr.append(scaling)

        loss_arr.append(statistics.mean(loss_runs))
        loss_std_arr.append(statistics.variance(loss_runs))
        print('\r', end="", flush=True)

        print(f"{i} - {statistics.mean(loss_runs):.1e} +/- {statistics.stdev(loss_runs):.1e}")

df = pandas.DataFrame({"learning_rate": lr_arr,
                       "Layers": ms_arr,
                       "Scaling": sc_arr,
                       "Points": nx_arr,
                       "Loss": loss_arr,
                       "Loss_std": loss_std_arr})

df.to_pickle("random_search.pkl")

print("Optimal:", i)
