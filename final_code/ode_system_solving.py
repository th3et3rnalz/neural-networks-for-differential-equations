import os

# The below environment variable is needed for my machine to get around an Out Of Memory (OOM) issue.
# Feel free to remove.
os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

import matplotlib.pyplot as plt
import numpy as onp
from jax.nn import sigmoid
import jax.numpy as np
from jax import vmap, hessian, jit, random, value_and_grad, jacobian
from random import randint
import optax


# ==================================================================================================================
# ===================== PROBLEM SPECIFIC SETUP ===========================
# ==================================================================================================================
def ode_func(y1_x, y2_x, y1, y2, x):
    return [y1_x - np.cos(x) - y1 * y1 - y2 + (1 + x * x + np.sin(x) ** 2),
            y2_x - 2 * x + (1 + x * x) * np.sin(x) - y1 * y2]


def bc_func(x, y1, y2):
    return [x*y1,
            1 + x*y2]


# ==================================================================================================================
# ===================== GENERAL NEURAL NETWORK FUNCTIONS ===========================
# ==================================================================================================================
def forward_pass(params, x_0):
    x = x_0.reshape(1, -1)

    for w, b in params[: -1]:
        x = sigmoid(np.dot(w, x) + b)

    final_w, final_b = params[-1]
    y_hat = np.dot(final_w, x) + final_b
    res = np.empty_like(y_hat)
    bc_ed = bc_func(x_0, y1=y_hat[0, :], y2=y_hat[1, :])
    res = res.at[0, :].set(bc_ed[0])
    res = res.at[1, :].set(bc_ed[1])
    return res


batch_forward = vmap(forward_pass, in_axes=(None, 0), out_axes=0)


def loss(w_b, x):
    y = batch_forward(w_b, x)
    y1 = y[0, 0, :]
    y2 = y[0, 1, :]

    y_x = vmap(fun=jacobian(batch_forward, 1), in_axes=(None, 0))(w_b, x.T)
    y1_x = y_x[:, 0, 0, 0, 0]
    y2_x = y_x[:, 0, 1, 0, 0]

    return np.mean(np.square(np.hstack(ode_func(y1_x=y1_x, y2_x=y2_x, y1=y1, y2=y2, x=x))))


def initialize_nn_wb(sizes, key):
    keys = random.split(key, len(sizes))

    def initialize_layer(m, n, key, scale=1e-2):
        w_key, b_key = random.split(key)
        weight_matrix = scale * random.normal(w_key, (n, m))
        bias_vector = scale * random.normal(b_key, (n, 1))

        return weight_matrix, bias_vector

    params = []
    for i in range(len(sizes) - 1):
        m = sizes[i]
        n = sizes[i + 1]
        params.append(initialize_layer(m=m, n=n, key=keys[i]))

    return params


@jit  # Adam optimizer
def update(w_b, x, opt_state):
    """Compute the gradient for a batch and update the parameters"""
    loss_v, grads = value_and_grad(loss)(w_b, x)
    updates, opt_state = optimizer.update(grads, opt_state, w_b)
    params = optax.apply_updates(w_b, updates)
    return params, opt_state, loss_v


# ==================================================================================================================
# ===================== GETTING GOING WITH THE INTERESTING STUFF ===========================
# ==================================================================================================================

def train_neural_network(optimizer, epochs, x, layers=(1, 10, 1), debug=False):
    key = random.PRNGKey(randint(0, 2 ** 16 - 1))
    w_b = initialize_nn_wb(sizes=layers, key=key)
    loss_history = []

    opt_state = optimizer.init(w_b)

    for i in range(epochs):
        w_b, opt_state, loss_v = update(w_b=w_b, x=x, opt_state=opt_state)
        if debug and i % 50 == 0:
            print(loss_v)
        loss_history.append(loss_v)

    return loss_history, w_b


if __name__ == "__main__":
    optimizer = optax.adam(0.00001)
    x = np.linspace(0, 2, 128, dtype=np.float32).reshape(1, -1)

    l_h, w_b = train_neural_network(optimizer=optimizer,
                                    epochs=500 * 1000,
                                    x=x,
                                    layers=[1, 20, 20, 2],
                                    debug=False)

    y = batch_forward(w_b, x)
    y1 = onp.array(y[0, 0, :])
    y2 = onp.array(y[0, 1, :])
    x = x.reshape(-1)

    plt.subplot(2, 3, 1)
    plt.plot(x, y1, label=r"$(\phi_1)_{NN}$")
    plt.plot(x, np.sin(x), label=r"$(\phi_1)_{True}$")
    plt.ylabel(r"$\phi_1(x)$")
    plt.legend()

    plt.subplot(2, 3, 2)
    plt.plot(x, y2, label=r"$(\phi_2)_{NN}$")
    plt.plot(x, 1 + x * x, label=r"$(\phi_2)_{True}$")
    plt.legend()

    plt.subplot(2, 3, 4)
    plt.semilogy(x, np.abs(y1 - np.sin(x)), label=r"$E_{\phi_1}$")
    plt.xlabel(r"x")
    plt.ylabel(r"Error $\phi_1$")
    plt.legend()

    plt.subplot(2, 3, 5)
    plt.semilogy(x, np.abs(y2 - (1 + x*x)), label=r"$E_{\phi_2}$")
    plt.legend()

    plt.subplot(1, 3, 3)
    plt.loglog(l_h, label="Training loss")
    plt.ylabel("Loss")
    plt.xlabel("Epochs")
    plt.legend()

    plt.tight_layout()
    plt.show()
