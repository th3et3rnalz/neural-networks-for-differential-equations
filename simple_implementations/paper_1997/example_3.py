import os
# The below environment variable is needed for my machine to get around an Out Of Memory (OOM) issue.
# Feel free to remove.
os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

from neural_de_solver import ODESolver, LinearLayer, Adam, LineSearchBacktrackingArmijo
import jax.nn as f_act
import jax.numpy as jnp
import numpy as np


layers = [LinearLayer(1, activation=f_act.sigmoid),
          LinearLayer(10, activation=f_act.sigmoid),
          LinearLayer(1)]


def ode_func(y_xx, y_x, y, x):
    return y_xx + y_x/5 + y + jnp.exp(-x/5)*jnp.cos(x)/5


def bc_func(x, y):
    return x + x*x*y


# optimizer = LineSearchBAMomentum(alpha_0=0.1, beta_ba=0.5, beta_momentum=0.1)
optimizer = LineSearchBacktrackingArmijo(alpha_0=0.1, beta_ba=0.5)
nn = ODESolver(layers=layers, order=2, ode_func=ode_func, bc_func=bc_func,
               title="Paper1997_ex_3", optimizer=optimizer, store_data=True, sentry_api_key="default", use_wandb=False)


x_train = np.linspace(0, 2, 4096).reshape(-1, 1)
x_test = np.linspace(0, 2, 8192).reshape(-1, 1)
n_iterations = 1000


print("Iter - E_train  - E_test-E_train")
print("================================")
for i in range(n_iterations):
    loss_i, alpha_i = nn.learn(x_train, step_n=i)

    if i % 10 == 0:
        loss_test = nn.loss(x_test)
        # nn.wandb.log({"test_loss": loss_test}, step=i)
        print(f"{i:4} : {loss_i:0.2e} : {loss_test-loss_i:0.2e}")

nn.plot_result(x=x_test, y_exact=jnp.exp(-x_test/5)*jnp.sin(x_test))

print("Finished training. See wandb instance for results (if initialized on this machine).")
