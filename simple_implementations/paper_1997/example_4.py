from neural_de_solver import LinearLayer, Adam, ODESystemSolver
import jax.numpy as jnp
import jax.nn as f_act


def ode_system_func(y1_x, y2_x, y1, y2, x):
    return jnp.array([y1_x - jnp.cos(x) - y1 * y1 - y2 + (1 + x * x + jnp.power(jnp.sin(x), 2)),
                      y2_x - 2 * x + (1 + x * x) * jnp.sin(x) + y1 * y2])


def bc_func(y, x):
    y_1 = y[:, 0]
    y_2 = y[:, 1]
    return jnp.array([x.T * y_1,
                      1 + x.T*y_2])[:, 0, :]


layers = [LinearLayer(1, activation=f_act.sigmoid),
          LinearLayer(10, activation=f_act.sigmoid),
          # LinearLayer(10, activation=f_act.sigmoid),
          LinearLayer(2, activation=f_act.sigmoid)]

opt = Adam(alpha=0.1)
nn = ODESystemSolver(layers=layers, bc_func=bc_func, ode_func=ode_system_func, use_wandb=False, title="TryoutPDE",
                     optimizer=opt, order=1)

x_train = jnp.linspace(0, 2, 1024).reshape(-1, 1)

a = nn.forward(x_train)
print(a)

nn.learn(x_train)
