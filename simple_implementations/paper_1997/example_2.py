from neural_de_solver import ODESolver, LinearLayer, LineSearchBAMomentum
import jax.nn as f_act
import jax.numpy as jnp
import numpy as np


layers = [LinearLayer(1, activation=f_act.sigmoid),
          LinearLayer(10, activation=f_act.sigmoid),
          LinearLayer(1)]


def ode_func(y_x, y, x):
    return y_x + y/5 - jnp.exp(-x/5)*jnp.cos(x)


def bc_func(x, y):
    return x*y


optimizer = LineSearchBAMomentum(alpha_0=0.1, beta_ba=0.5, beta_momentum=0.1)
nn = ODESolver(layers=layers, order=1, ode_func=ode_func, bc_func=bc_func,
               title="Paper1997_ex_2", optimizer=optimizer, store_data=True)


x_train = np.linspace(0, 2, 1024).reshape(-1, 1)
x_test = np.linspace(0, 2, 4096).reshape(-1, 1)
n_iterations = 1000


print("Iter - E_train  - E_test-E_train")
print("================================")
for i in range(n_iterations):
    loss_i, alpha_i = nn.learn(x_train)

    if i % 10 == 0:
        loss_test = nn.loss(x_test)
        print(f"{i:4} : {loss_i:0.2e} : {loss_test-loss_i:0.2e}")

nn.plot_result(x=x_test, y_exact=jnp.exp(-x_test/5)*jnp.sin(x_test))

print("Finished training. See wandb instance for results (if initialized on this machine).")
