import os
# The below environment variable is needed for my machine to get around an Out Of Memory (OOM) issue.
# Feel free to remove.
os.environ['XLA_PYTHON_CLIENT_PREALLOCATE'] = 'false'

from neural_de_solver import ODESolver, LinearLayer, LineSearchBAMomentum
import jax.nn as f_act
import numpy as np


layers = [LinearLayer(1, activation=f_act.sigmoid),
          LinearLayer(10, activation=f_act.sigmoid),
          LinearLayer(1)]


def ode_func(y_x, y, x):
    return y_x + (x + (1+3*x*x) / (1+x+x*x*x))*y - x*x*x - 2*x - x*x*((1+3*x*x) / (1+x+x*x*x))


def bc_func(x, y):
    return 1 + x*y


optimizer = LineSearchBAMomentum(alpha_0=0.1, beta_ba=0.5, beta_momentum=0.1)
nn = ODESolver(layers=layers, order=1, ode_func=ode_func, bc_func=bc_func,
               title="Paper1997_ex_1", optimizer=optimizer, store_data=True, use_wandb=False)


x_train = np.linspace(0, 2, 1024).reshape(-1, 1)
x_test = np.linspace(0, 2, 4096).reshape(-1, 1)
n_iterations = 500


print("Iter - E_train  - E_test-E_train")
print("================================")
for i in range(n_iterations):
    loss_i, alpha_i = nn.learn(x_train)

    if i % 10 == 0:
        loss_test = nn.loss(x_test)
        print(f"{i:4} : {loss_i:0.2e} : {loss_test-loss_i:0.2e}")

nn.plot_result(x=x_train, y_exact=x_train*x_train + np.exp(-x_train*x_train*0.5) / (1 + x_train + np.power(x_train, 3)))

print("Finished training. See wandb instance for results (if initialized on this machine).")
